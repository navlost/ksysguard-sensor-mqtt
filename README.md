# MQTT bridge for the KDE System Monitor (ksysguard)

A short Python script for monitoring arbitrary MQTT topics via [ksysguard](https://userbase.kde.org/KSysGuard).

# Install

Save the script somewhere sensible in your computer and give it executable permissions. Root is not required.

# Configure

Take a look the [example configuration file](./example-config.yaml) and use it as the basis for your own configuration.

# Use

Open ksysguard and go to `File` → `Monitor Remote Machine…`. In the dialog box, enter an arbitrary name under “Host”, select “custom command” and in the command edit box enter the full path to the script in your computer, followed by `-c` (or `--config`) and the full path to your configuration file. For instance:

```
/home/navlost/bin/ksysguard-sensor-mqtt.py -c /home/navlost/.config/ksysguard-sensor-mqtt.yaml
```

![](./doc/connect-host.png)

Ensure that the sensor browser on the right-hand side of the ksysguard client is visible, if not, slide it into view by clicking near (but not on) the right-hand edge of the window and dragging left. You should now see your configured sensors and be able to drag them into a tab.

![](./doc/ksysguard-window.png)
